void Toggle_HUD(int iClient)
{
	if(g_iHudMode[iClient] == HUD_DISABLED)
	{
		g_iHudMode[iClient] = HUD_ALL;
		CPrintToChat(iClient, "%s %t", PREFIX, "HUD Tutorial all");
	}
	else if(g_iHudMode[iClient] == HUD_ALL)
	{
		g_iHudMode[iClient] = HUD_NORMAL;
		CPrintToChat(iClient, "%s %t", PREFIX, "HUD Tutorial normal");
	}
	else if(g_iHudMode[iClient] == HUD_NORMAL)
	{
		g_iHudMode[iClient] = HUD_IMPORTANT;
		CPrintToChat(iClient, "%s %t", PREFIX, "HUD Tutorial important");
	}
	else
	{
		g_iHudMode[iClient] = HUD_DISABLED;
		CPrintToChat(iClient, "%s %t", PREFIX, "HUD Tutorial disabled");
	}
	
	char sBuffer[8];
	IntToString(g_iHudMode[iClient], sBuffer, sizeof(sBuffer));
	SetClientCookie(iClient, g_hCookieHudMode, sBuffer);
}

void StartHUDTimer()
{
	CreateTimer(1.0, Timer_HUD, _, TIMER_REPEAT);
}

public Action Timer_HUD(Handle timer, any data)
{
	LoopIngamePlayers(iClient)
	{
		if(!IsPlayerAlive(iClient))
			continue;
		
		if(GetClientTeam(iClient) == CS_TEAM_T)
		{
			HUD_Countdown(iClient);
			HUD_Points(iClient);
			HUD_ShopCooldown(iClient);
			HUD_HiderInfo(iClient);
			HUD_HiderHelp(iClient);
		}
		
		else if(GetClientTeam(iClient) == CS_TEAM_CT)
		{
			HUD_Countdown(iClient);
			HUD_Points(iClient);
			HUD_ShopCooldown(iClient);
			HUD_SeekerInfo(iClient);
			HUD_SeekerHelp(iClient);
		}
	}
	
	return Plugin_Continue;
}

void HUD_Countdown(int iClient)
{
	int iHideTime = PH_CanChangeModel();
	if(iHideTime > 0)
	{
		char sBuffer[512];
		Format(sBuffer, sizeof(sBuffer), "Hide Time: %is", iHideTime);
		ShowGameText(iClient, 2, { 255, 55, 55, 255 }, -1.0, 0.35, 1.1, sBuffer);
	}
}

void HUD_Points(int iClient)
{
	char sBuffer[512];
	
	static float fPoints[MAXPLAYERS + 1];
	float fDiff = g_fPoints[iClient] - fPoints[iClient];
	int iDiff = RoundToFloor(fDiff);
	if (fDiff <= -1.0)
	{
		Format(sBuffer, sizeof(sBuffer), "- %i", -iDiff);
		ShowGameText(iClient, 2, { 255, 55, 55, 255 }, -1.0, 0.45, 1.1, sBuffer);
	}
	if (iDiff >= 9.0)
	{
		Format(sBuffer, sizeof(sBuffer), "+%i", iDiff);
		ShowGameText(iClient, 2, { 55, 255, 55, 255 }, -1.0, 0.45, 1.1, sBuffer);
	}
	fPoints[iClient] = g_fPoints[iClient];
}

void HUD_ShopCooldown(int iClient)
{
	char sBuffer[512];
	Format(sBuffer, sizeof(sBuffer), "Points: $%i", RoundToFloor(g_fPoints[iClient]));
	
	int iTeam = GetClientTeam(iClient);
	
	int iCount;
	for (int iItem = 0; iItem < g_aShopName.Length; iItem++)
	{
		// Check team
		int iTeam2 = g_aShopTeam.Get(iItem);
		
		if(iTeam != iTeam2 && iTeam2 > CS_TEAM_SPECTATOR)
			continue;
		
		char sName[32];
		g_aShopName.GetString(iItem, sName, sizeof(sName));
		
		// Item disabled for all players
		if(g_aShopItemDisabled.FindString(sName) != -1)
			continue;
		
		// Item disabled for this player
		if(g_aClientShopItemDisabled[iClient].FindString(sName) != -1)
			continue;
		
		// Item unlocked by round time
		int iUnlockTime = g_aShopUnlockTime.Get(iItem);
		int iCountdown = iUnlockTime - (GetTime() - g_iRoundStart);
		bool bUnlocked = iUnlockTime <= GetTime() - g_iRoundStart;
		
		if(bUnlocked || iCountdown > 10)
			continue;
		
		Format(sBuffer, sizeof(sBuffer), "%s\n%is - %s", sBuffer, iCountdown, sName);
		
		iCount++;
	}
	
	ShowGameText(iClient, 3, { 255, 255, 55, 255 }, 0.01, 0.27, 1.1, sBuffer);
}

void HUD_HiderInfo(int iClient)
{
	
}

void HUD_HiderHelp(int iClient)
{
	
}

void HUD_SeekerInfo(int iClient)
{
	
}

void HUD_SeekerHelp(int iClient)
{
	
}