void RegisterItems()
{
	RegisterShopItem("Bonus Health", 		CS_TEAM_T, g_cvShopHiderHealPrice.IntValue, 			g_cvShopHiderHealSort.IntValue, 			g_cvShopHiderHealUnlockTime.IntValue, false);
	RegisterShopItem("Morph", 				CS_TEAM_T, g_cvShopHiderMorphPrice.IntValue, 			g_cvShopHiderMorphSort.IntValue, 		g_cvShopHiderMorphUnlockTime.IntValue, false);
	RegisterShopItem("Freeze Air Upgrade", 	CS_TEAM_T, g_cvShopHiderAirFreezePrice.IntValue, 		g_cvShopHiderAirFreezeSort.IntValue, 	g_cvShopHiderAirFreezeUnlockTime.IntValue, false);
	
	RegisterShopItem("Healthshot",			 CS_TEAM_CT, g_cvShopSeekerHealthshotPrice.IntValue, 	g_cvShopSeekerHealthshotSort.IntValue, 	g_cvShopSeekerHealthshotUnlockTime.IntValue, false);
	RegisterShopItem("Grenade", 			CS_TEAM_CT, g_cvShopSeekerGrenadePrice.IntValue, 		g_cvShopSeekerGrenadeSort.IntValue, 	g_cvShopSeekerGrenadeUnlockTime.IntValue, false);
	RegisterShopItem("Taser", 				CS_TEAM_CT, g_cvShopSeekerTaserPrice.IntValue, 			g_cvShopSeekerTaserSort.IntValue, 		g_cvShopSeekerTaserUnlockTime.IntValue, false);
	RegisterShopItem("FiveSeven", 			CS_TEAM_CT, g_cvShopSeekerFiveSevenPrice.IntValue, 		g_cvShopSeekerFiveSevenSort.IntValue, 	g_cvShopSeekerFiveSevenUnlockTime.IntValue, false);
	RegisterShopItem("XM1014", 			CS_TEAM_CT, g_cvShopSeekerXM1014Price.IntValue, 		g_cvShopSeekerXM1014Sort.IntValue, 	g_cvShopSeekerXM1014UnlockTime.IntValue, false);
	RegisterShopItem("MP9", 				CS_TEAM_CT, g_cvShopSeekerMP9Price.IntValue, 			g_cvShopSeekerMP9Sort.IntValue, 		g_cvShopSeekerMP9UnlockTime.IntValue, false);
	RegisterShopItem("M4A1", 				CS_TEAM_CT, g_cvShopSeekerM4A1Price.IntValue,			g_cvShopSeekerM4A1Sort.IntValue, 		g_cvShopSeekerM4A1UnlockTime.IntValue, false);
	RegisterShopItem("AWP", 				CS_TEAM_CT, g_cvShopSeekerAWPPrice.IntValue, 			g_cvShopSeekerAWPSort.IntValue, 		g_cvShopSeekerAWPUnlockTime.IntValue, false);
}

public Action PH_OnBuyShopItem(int iClient, char[] sName, int &points)
{
	if(StrEqual(sName, "Bonus Health"))
	{
		int iHealth = GetClientHealth(iClient);
		int iNewHealth = iHealth + g_cvShopHiderHealAmount.IntValue;
		
		if(iNewHealth > g_cvShopHiderHealMax.IntValue)
			iNewHealth = g_cvShopHiderHealMax.IntValue;
			
		SetEntityHealth(iClient, iNewHealth);
		
		CPrintToChat(iClient, "%s %t", PREFIX , "Bonus Health", iNewHealth - iHealth);
		
		return Plugin_Handled;
	}
	else if(StrEqual(sName, "Morph"))
	{
		SetModel(iClient);
		return Plugin_Handled;
	}
	else if(StrEqual(sName, "Freeze Air Upgrade"))
	{
		CPrintToChat(iClient, "%s %t", PREFIX , "Freeze Air Upgrade");
		PH_DisableShopItem("Freeze Air Upgrade", iClient);
		g_bUpgradeFreezeAir[iClient] = true;
		return Plugin_Handled;
	}
	else if(StrEqual(sName, "Healthshot"))
	{
		GivePlayerItem(iClient, "weapon_healthshot");
		return Plugin_Handled;
	}
	else if(StrEqual(sName, "Grenade"))
	{
		GivePlayerItem(iClient, "weapon_hegrenade");
		return Plugin_Handled;
	}
	else if(StrEqual(sName, "Taser"))
	{
		GivePlayerItem(iClient, "weapon_taser");
		return Plugin_Handled;
	}
	else if(StrEqual(sName, "FiveSeven"))
	{
		GivePlayerItem(iClient, "weapon_fiveseven");
		PH_DisableShopItem("FiveSeven", iClient);
		return Plugin_Handled;
	}
	else if(StrEqual(sName, "XM1014"))
	{
		GivePlayerItem(iClient, "weapon_xm1014");
		PH_DisableShopItem("XM1014", iClient);
		return Plugin_Handled;
	}
	else if(StrEqual(sName, "MP9"))
	{
		GivePlayerItem(iClient, "weapon_mp9");
		PH_DisableShopItem("MP9", iClient);
		return Plugin_Handled;
	}
	else if(StrEqual(sName, "M4A1"))
	{
		GivePlayerItem(iClient, "weapon_m4a1");
		PH_DisableShopItem("AWP", iClient);
		return Plugin_Handled;
	}
	else if(StrEqual(sName, "AWP"))
	{
		GivePlayerItem(iClient, "weapon_awp");
		PH_DisableShopItem("AWP", iClient);
		return Plugin_Handled;
	}
	
	return Plugin_Continue;
}