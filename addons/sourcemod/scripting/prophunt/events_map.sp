public void OnMapStart()
{
	ReadModelConfig();
	ReadMapModelConfig();
	LoadTauntSoundPacks();
	StartNoBlockTimer();
	
	g_iRoundStart = 0;
}

public void OnMapEnd()
{
	for(int i = 0; i <= MaxClients; i++)
		delete g_mModelMenu[i];
	delete g_kvModels;
	
	g_iRoundStart = 0;
	
	delete g_hRoundTimeTimer;
	delete g_hAfterFreezeTimer;
	delete g_hRoundEndTimer;
	
	LoopClients(i)
		delete g_hAutoFreezeTimers[i];
}

